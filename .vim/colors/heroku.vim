" ======================================================================
" Heroku
" ======================================================================
" A Sublime Text 2 / Textmate theme.
" Copyright (c) 2014 Dayle Rees.
" Modified by Feifei Hang.
" Released under the MIT License <http://opensource.org/licenses/MIT>
" ======================================================================
" Find more themes at : https://github.com/daylerees/colour-schemes
" ======================================================================

set background=dark
" set t_Co=256
set nocursorline
hi clear
syntax reset

" User defined colors
hi User1 ctermfg=215    ctermbg=234 guibg=bg guifg=#FFAF5F
hi User2 ctermfg=88     ctermbg=234 guibg=bg guifg=#870000
hi User3 ctermfg=60     ctermbg=234 guibg=bg guifg=#5F5F87
hi User4 ctermfg=119    ctermbg=234 guibg=bg guifg=#87FF5F
hi User5 ctermfg=226    ctermbg=234 guibg=bg guifg=#FFFF00

" Colors for the User Interface.

hi Cursor      guibg=#cc4455  guifg=white     ctermbg=4 ctermfg=15
hi link CursorIM Cursor
hi Normal      guibg=#1b1b24  guifg=#c8c7d5    gui=none ctermbg=0 ctermfg=15
hi NonText     guibg=bg  guifg=#c8c7d5   ctermbg=8 ctermfg=14
hi Visual      guibg=#557799  guifg=white    gui=none ctermbg=9 ctermfg=15

hi Linenr      guibg=bg       guifg=#aaaaaa  gui=none ctermbg=bg ctermfg=7

hi Directory   guibg=bg       guifg=#337700  gui=none ctermbg=bg ctermfg=10

hi IncSearch   guibg=#0066cc  guifg=white    gui=none ctermbg=1 ctermfg=15
hi link Seach IncSearch

hi SpecialKey  guibg=bg guifg=fg       gui=none ctermbg=bg ctermfg=fg
hi Titled      guibg=bg guifg=fg       gui=none ctermbg=bg ctermfg=fg

hi ErrorMsg    guibg=bg guifg=#ff0000   ctermbg=bg ctermfg=12
hi ModeMsg     guibg=bg guifg=#ffeecc  gui=none ctermbg=bg ctermfg=14
hi link  MoreMsg     ModeMsg
hi Question    guibg=bg guifg=#585480   ctermbg=bg ctermfg=10
hi link  WarningMsg  ErrorMsg

hi StatusLine     guibg=bg  guifg=#585480     ctermbg=14 ctermfg=0
" hi StatusLineNC   guibg=#7873ae  guifg=black    gui=none ctermbg=4  ctermfg=11
hi VertSplit      guibg=#7873ae  guifg=black    gui=none ctermbg=4  ctermfg=11

hi DiffAdd     guibg=#446688  guifg=fg    gui=none ctermbg=1 ctermfg=fg
hi DiffChange  guibg=#558855  guifg=fg    gui=none ctermbg=2 ctermfg=fg
hi DiffDelete  guibg=#884444  guifg=fg    gui=none ctermbg=4 ctermfg=fg
hi DiffText    guibg=#884444  guifg=fg     ctermbg=4 ctermfg=fg

" Colors for Syntax Highlighting.

hi Comment  guibg=bg  guifg=#505067  gui=none    ctermbg=8   ctermfg=7

hi Constant    guibg=bg    guifg=white    ctermbg=8   ctermfg=15
hi String      guibg=bg    guifg=#a6fa62  ctermbg=bg  ctermfg=14
hi Character   guibg=bg    guifg=#7873ae  ctermbg=bg  ctermfg=14
hi Number      guibg=bg    guifg=#a6fa62  ctermbg=1   ctermfg=15
hi Boolean     guibg=bg    guifg=#a6fa62  ctermbg=1   ctermfg=15 gui=none
hi Float       guibg=bg    guifg=#a6fa62  ctermbg=1   ctermfg=15

" hi Identifier  guibg=bg    guifg=#c8c7d5      ctermbg=bg  ctermfg=12
hi Identifier  guibg=bg    guifg=#7873ae      ctermbg=bg  ctermfg=12
hi Function    guibg=bg    guifg=#7873ae      ctermbg=bg  ctermfg=12
hi Statement   guibg=bg    guifg=#7873ae      ctermbg=bg  ctermfg=14

hi Conditional guibg=bg    guifg=#7873ae      ctermbg=bg  ctermfg=12
hi Repeat      guibg=bg    guifg=#7873ae      ctermbg=4   ctermfg=14
hi Label       guibg=bg    guifg=#ffccff      ctermbg=bg   ctermfg=13
hi Operator    guibg=bg    guifg=#7873ae      ctermbg=6   ctermfg=15
hi Keyword     guibg=bg    guifg=#7873ae      ctermbg=bg  ctermfg=10
hi Exception   guibg=bg    guifg=#7873ae      ctermbg=bg  ctermfg=10

hi PreProc    guibg=bg   guifg=#ffcc99   ctermbg=4  ctermfg=14
hi Include    guibg=bg   guifg=#585480   ctermbg=bg ctermfg=10
hi link Define    Include
hi link Macro     Include
hi link PreCondit Include

hi   Type             guibg=bg        guifg=#585480   ctermbg=bg      ctermfg=12
hi   StorageClass     guibg=bg        guifg=#7873ae   ctermbg=bg      ctermfg=10
hi   Structure        guibg=bg        guifg=#c8c7d5   ctermbg=bg      ctermfg=10
hi   Typedef          guibg=bg        guifg=#585480   ctermbg=bg      ctermfg=10

hi   Special          guibg=bg        guifg=#bbddff   ctermbg=1       ctermfg=15
hi   SpecialChar      guibg=bg        guifg=#bbddff   ctermbg=1       ctermfg=15
hi   Tag              guibg=bg        guifg=#bbddff   ctermbg=1       ctermfg=15
hi   Delimiter        guibg=bg        guifg=fg        ctermbg=1       ctermfg=fg
hi   SpecialComment   guibg=#334455   guifg=#5d5d76   ctermbg=1       ctermfg=15
hi   Debug            guibg=bg        guifg=#ff9999   gui=none        ctermbg=8    ctermfg=12

hi   Underlined       guibg=bg        guifg=#99ccff   gui=underline   ctermbg=bg   ctermfg=9    cterm=underline

hi   Title    guibg=bg   guifg=#c8c7d5   ctermbg=1     ctermfg=15
hi   Ignore   guibg=bg   guifg=#cccccc   ctermbg=bg    ctermfg=8
"    hi       Error      guibg=#ff0000   guifg=white   ctermbg=12   ctermfg=15
hi   Error    guibg=bg   guifg=white     ctermbg=12    ctermfg=15
hi   Todo     guibg=bg   guifg=#ff0000   gui=bold      ctermbg=1    ctermfg=12

hi   Folded     guibg=#334455   guifg=#bbddff   gui=bold
hi   Pmenu      guibg=#7874ae   guifg=#a6fa62
hi   PmenuSel   guibg=#a6fa62   guifg=#ff0000   gui=bold
hi   CursorLine      guibg=#334455
hi   MatchParen guibg=#a6fa62   guifg=black  gui=bold

hi htmlH2 guibg=bg guifg=fg  ctermbg=8 ctermfg=fg
hi link htmlH3 htmlH2
hi link htmlH4 htmlH3
hi link htmlH5 htmlH4
hi link htmlH6 htmlH5

" And finally.

let g:colors_name = "Heroku"
let colors_name   = "Heroku"
