" Vim color file
" Maintainer:  Feifei Hang (l.daniel.hung@gmail.com)
" Last Change:  Sun 23-02-2014 08:15 pm
"

set background=dark

if version > 580
 hi clear
 if exists("syntax_on")
  syntax reset
 endif
endif

let colors_name = "ninja"

" User defined colors
hi User1 ctermfg=215    ctermbg=234
hi User2 ctermfg=88     ctermbg=234
hi User3 ctermfg=60     ctermbg=234
hi User4 ctermfg=119    ctermbg=234
hi User5 ctermfg=226    ctermbg=234

" General colors
hi Normal          ctermfg=white      ctermbg=NONE     cterm=none  gui=none
hi Cursor          ctermfg=0        ctermbg=228      cterm=none  gui=none
hi Visual          ctermfg=white    ctermbg=74       cterm=none  gui=none
hi VisualNOS       ctermfg=251      ctermbg=236      cterm=none  gui=none
hi Search          ctermfg=0        ctermbg=226      cterm=none  gui=none
hi Folded          ctermfg=103      ctermbg=237      cterm=none  gui=none
hi Title           ctermfg=white      cterm=none       gui=bold
hi StatusLine      ctermfg=230      ctermbg=238      cterm=none  gui=italic
hi VertSplit       ctermfg=238      ctermbg=238      cterm=none  gui=none
hi StatusLineNC    ctermfg=241      ctermbg=238      cterm=none  gui=none
hi SpecialKey      ctermfg=241      ctermbg=235      cterm=none  gui=none
hi WarningMsg      ctermfg=203
hi ErrorMsg        ctermfg=196      ctermbg=236      cterm=bold  gui=bold
hi Error           ctermbg=NONE

" Vim >= 7.0 specific colors
if version >= 700
	hi CursorLine ctermbg=235  cterm=none     
	hi MatchParen ctermfg=60   ctermbg=119  cterm=bold  gui=bold
	hi Pmenu      ctermfg=230  ctermbg=238    
	hi PmenuSel   ctermfg=232  ctermbg=192    
endif

" Diff highlighting
hi DiffAdd       ctermfg=white    ctermbg=25  
hi DiffDelete    ctermfg=white    ctermbg=88  gui=none
hi DiffText      ctermbg=53       cterm=none  gui=none
hi DiffChange    ctermbg=237

"hi CursorIM
"hi Directory
"hi IncSearch
"hi Menu
hi ModeMsg ctermbg=NONE
"hi MoreMsg
"hi PmenuSbar
"hi PmenuThumb
"hi Question
"hi Scrollbar
" hi SignColumn ctermbg=NONE cterm=bold
hi SpellBad ctermbg=NONE cterm=underline
hi SpellCap ctermbg=NONE cterm=underline
"hi SpellLocal
"hi SpellRare
"hi TabLine
"hi TabLineFill
"hi TabLineSel
" hi Tooltip ctermbg=NONE
"hi User1
"hi User9
"hi WildMenu


" Syntax highlighting
hi Keyword    ctermfg=111    cterm=none     gui=none
hi Statement  ctermfg=60     cterm=none     gui=none
hi Constant   ctermfg=119    cterm=none     gui=none
hi Number     ctermfg=119    cterm=none     gui=none
hi PreProc    ctermfg=180    cterm=none     gui=none
hi Function   ctermfg=60     cterm=none     gui=none
hi Identifier ctermfg=60     cterm=none     gui=none
hi Type       ctermfg=60     cterm=none     gui=none
hi Special    ctermfg=110    cterm=none     gui=none
hi String     ctermfg=119    cterm=none     gui=italic
hi Comment    ctermfg=240    cterm=none     gui=italic
hi Todo       ctermfg=88     ctermbg=none   cterm=bold  gui=italic


" Links
" hi! link FoldColumn  Folded
" hi! link CursorColumn CursorLine
" hi! link NonText  LineNr

highlight clear SignColumn      " SignColumn should match background
highlight clear LineNr          " Current line number row will have same background color in relative mode

" vim:set ts=4 sw=4 noet:
