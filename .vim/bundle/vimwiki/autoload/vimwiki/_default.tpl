<!DOCTYPE HTML>
<html>
<head>
<link rel="Stylesheet" type="text/css" href="%root_path%%css%">
<link type="text/css" rel="stylesheet" href="%root_path%templates/SyntaxHighlighter/styles/shCore.css" /> 
<link type="text/css" rel="stylesheet" href="%root_path%templates/SyntaxHighlighter/styles/shThemeDefault.css" /> 
<title>%title%</title>
<meta http-equiv="Content-Type" content="text/html; charset=%encoding%">

<!-- SyntaxHighlighter -->
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shCore.js"></script> 
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushPython.js"></script>
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushCss.js"></script>
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushJScript.js"></script>
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushCpp.js"></script>
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushPhp.js"></script>
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushXml.js"></script>
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushSql.js"></script>
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushBash.js"></script>
<script type="text/javascript" src="%root_path%templates/SyntaxHighlighter/scripts/shBrushPlain.js"></script>
<script type="text/javascript">
SyntaxHighlighter.all();
</script> 

</head>
<body>
%content%
</body>
</html>
