" Global settings " {{{
set nocompatible           " always use Vim mode, not Vi
set encoding=utf-8         " use UTF-8 as the default charset

set mouse=a                " enable mouse
set virtualedit=onemore    " Allow for cursor beyond last character
set ttyfast                " indicates that I'm using a fast terminal connection
if exists('$TMUX')
    set ttymouse=xterm2    " use xterm2 mouse mode when in Tmux
endif

set shortmess+=filmnrxoOtT " Ease 'Hit Enter to continue'

set noswapfile             " Damn you, swap files!

set cindent                " enable C-style indentation

set smartcase              " smart case sensitiveness

set incsearch              " search on the fly... well, as soon as typed

set lazyredraw             " do not keep redrawing during macros

set hidden                 " Allow buffer switching without saving

set showmatch              " show matching brackets
set mat=2                  " How many tenths of a second to blink when matching brackets

set re=1                   " force Vim to use the old Regex engine to address the slow buffer loading issue

set expandtab              " Use spaces instead of tabs

" 1 tab == 4 spaces
set tabstop=4
set shiftwidth=4
set ts=4 sw=4 et

" show line numbers
set nu

"enable syntax highlight"
syntax on

" show in which mode now
set showmode

" some GUI stuff
set guioptions-=T

" make system clipboard being the default register
set clipboard+=unnamed
set clipboard+=unnamedplus

" turn off god damn error/warning visual blink/beep
set vb

" CD to the directory of current file
set autochdir

" file tab
set wildmenu

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" show commands typed
set showcmd

" set terminal color
set t_Co=256

set linebreak          " For lines longer than the window, wrap intelligently.
                       " This doesn't insert hard line breaks.
set showbreak=⇇\       " string to put before wrapped screen lines
" set showbreak=↪\ 

" enable highlight search
set hlsearch

" enable syntax
syntax enable
filetype plugin on
filetype indent on
filetype plugin indent on

" dark background
set background=dark

" Only show 15 tabs
set tabpagemax=15

" Only highlight 120 columns
" set synmaxcol=120

" do not blink cursor
set guicursor+=a:blinkon0

set formatoptions=l

" line break on 500 characters
set lbr
" set tw=500

" fold codes on marker
set foldmethod=marker

" show annoying characters
set nolist
set listchars=tab:▸\ ,trail:•,extends:#,nbsp:. " Highlight problematic whitespace
" set listchars=tab:▸\ ,extends:#,nbsp:. " Highlight problematic whitespace

" backspace behaves normal
set backspace=indent,eol,start

" set max syntax columns to prevent lag on long lines (default: 3000)
" set synmaxcol=128

" Open new split windows to the right/bottom
set splitright splitbelow

" Remember info about open buffers on close
set viminfo^=%

" set up backup and undo stuffs
if has("persistent_undo")
    set undodir=~/.vim/tmp/undo//
    set backupdir=~/.vim/tmp/backup//
    set directory=~/.vim/tmp/swap//
    set backupskip=/tmp/*,/private/tmp/*

    set backup
    set writebackup
    set undofile

    set history=1000
    set undolevels=1000

endif

"}}}
"
" Bring in some stuffs"{{{
" bring in Pathogen
execute pathogen#infect()

" bring in SaveAs!
source ~/.vim/shorts/saveas.vim
"}}}
"
" Autocmd"{{{
if has('autocmd')
    autocmd BufNewFile,BufRead *.tex set spell
    autocmd BufNewFile,BufRead *.tex syn spell toplevel
    autocmd BufNewFile,BufRead *.rdf set filetype=xml
    autocmd BufNewFile,BufRead *.json set filetype=json
    autocmd BufNewFile,BufRead *.md set filetype=markdown

    " Git commits.
    autocmd FileType gitcommit setlocal spell

    " " auto reload .vimrc changes
    " augroup reload_vimrc " {
    "     autocmd!
    "     autocmd BufWritePost $MYVIMRC source $MYVIMRC
    " augroup END " }
    
    " only show special characters in source codes
    autocmd FileType * set nolist
    autocmd FileType c,cpp,java,go,php,javascript,json,css,python,twig,xml,yml,sh set list

    " auto remove trailing whitespace before saving
    autocmd FileType c,cpp,java,go,php,javascript,python,twig,xml,yml autocmd BufWritePre <buffer> :%s/\s\+$//e

    " Instead of reverting the cursor to the last position in the buffer, we
    " set it to the first line when editing a git commit message
    au FileType gitcommit au! BufEnter COMMIT_EDITMSG call setpos('.', [0, 1, 1, 0])

    " auto unfold files on open
    autocmd Syntax c,cpp,xml,html,xhtml,perl,json,javascript,python,php,java,markdown,pandoc,vimwiki normal zR

    " auto wrap at column 80
    autocmd Syntax text,markdown,pandoc setlocal tw=80 

    " use 2 spaces indentation for javascript, html, and css
    autocmd Filetype javascript,html,css setlocal ts=2 sts=2 sw=2

    " Open the quickfix window with the results of the grep command.
    autocmd QuickFixCmdPost *grep* cwindow

    " toggle cursorline when changing mode
    " autocmd InsertEnter,InsertLeave * set cul!
    
    " Javascript Libraries Syntax
    autocmd BufReadPre *.js let b:javascript_lib_use_jquery = 1
    autocmd BufReadPre *.js let b:javascript_lib_use_underscore = 1
    autocmd BufReadPre *.js let b:javascript_lib_use_backbone = 1
    autocmd BufReadPre *.js let b:javascript_lib_use_prelude = 0
    autocmd BufReadPre *.js let b:javascript_lib_use_angularjs = 1

    " NeoComplCache for Javascript
    autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS

endif
"}}}
"
" GUI Settings"{{{
if has("gui_running")
    " colorscheme wombat256mod
    colorscheme heroku
    set guifont=Source\ Code\ Pro\ for\ Powerline:h13
    set guioptions=aAc
    " set nocursorline

    set fuopt+=maxhorz " grow to maximum horizontal width on entering fullscreen mode
    " map :set invfu " toggle fullscreen mode
else
    colorscheme ninja
    set guifont=Source\ Code\ Pro\ for\ Powerline:h13
endif

highlight clear SignColumn      " SignColumn should match background
highlight clear LineNr          " Current line number row will have same background color in relative mode

if has('cmdline_info')
    set ruler                   " Show the ruler
    " set rulerformat=%30(%=\:b%n\ %y\ %<%F%m%r%w\ %l,%c%V\ %P%) " A ruler on steroids
    set showcmd                 " Show partial commands in status line and
    " Selected characters/lines in visual mode
endif

if has('statusline')
    set laststatus=2
    set statusline=
    set statusline +=%1*\ %n\ %*            "buffer number
    set statusline +=%3*%y%*                "file type
    set statusline +=%4*\ %<%f%*            "filename
    set statusline +=%2*\ %([%M%R%H%W]\ %)%* " Modified, Read-only, Help, and Preview flags
    set statusline +=%1*\ %{fugitive#statusline()}%* " Git Hotness

    set statusline+=%2*%=%{SyntasticStatuslineFlag()}\ %* " show Syntastic flag
    set statusline+=%* " back to normal color

    set statusline +=%4*LN:\ %l%*             "current line
    set statusline +=%3*/%L,\ %*               "total lines
    " set statusline +=%4*%4v\ %*             "virtual column number
    set statusline +=%4*%P\ %*                  "percentage

endif

"}}}

" Key bindings"{{{
" buffer managements "{{{
" nmap <leader>be :ls<CR>:buffer<Space>
" nmap <leader>bd :ls<CR>:bd<space>
nnoremap <leader>bd :Bclose<cr>
nnoremap <leader>ba :1,1000 bd!<cr>
"}}}
" text decorations: underlines, wrapped stars"{{{
nnoremap <leader>=  yypv$r=
nnoremap <leader>-  yypv$r-
nnoremap <leader>**  ciw****<esc>hhp
nnoremap <leader>*  ciw**<esc>hp
nnoremap <leader>_  ciw__<esc>hp

vnoremap <leader>** di**<esc>pa**<esc>
vnoremap <leader>* di*<esc>pa*<esc>
vnoremap <leader>_ di_<esc>pa_<esc>
"}}}
" Search for selected text, forwards or backwards."{{{
vnoremap <silent> * :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy/<C-R><C-R>=substitute(
  \escape(@", '/\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
vnoremap <silent> # :<C-U>
  \let old_reg=getreg('"')<Bar>let old_regtype=getregtype('"')<CR>
  \gvy?<C-R><C-R>=substitute(
  \escape(@", '?\.*$^~['), '\_s\+', '\\_s\\+', 'g')<CR><CR>
  \gV:call setreg('"', old_reg, old_regtype)<CR>
"}}}
" Map <Leader>ff to display all lines with keyword under cursor"{{{
" and ask which one to jump to
nmap <Leader>ff [I:let nr = input("Which one: ")<Bar>exe "normal " . nr ."[\t"<CR>
"}}}
" Find merge conflict markers"{{{
map <leader>fc /\v^[<\|=>]{7}( .*\|$)<CR>
"}}}
" Yank from the cursor to the end of the line, to be consistent with C and D."{{{
nnoremap Y y$
"}}}
" in case forgot to sudo"{{{
cmap w!! %!sudo tee > /dev/null %
"}}}
" map capital Q and W and etc."{{{
ca W w
ca Q q
ca WQ wq
ca Wq wq
"}}}
" use <tab> to find the matching brace"{{{
map <tab> %
"}}}
" enable <C-a> and <C-e> in command mode"{{{
cnoremap <c-a> <home>
cnoremap <c-e> <end>
"}}}
" navigate to the beginning or end of sentence"{{{
noremap L $
noremap H ^
"}}}
" Move through lines"{{{
noremap <buffer> <silent> k gk
noremap <buffer> <silent> j gj
noremap <Up> gk
noremap <Down> gj
"}}}
" Add current date and time"{{{
nmap <F3> a<C-R>=strftime("%a %d-%m-%Y %I:%M %p")<CR><Esc>
imap <F3> <C-R>=strftime("%a %d-%m-%Y %I:%M %p")<CR>
"}}}
" make be movements accessible from insert mode via the <Ctrl> modifier key"{{{
inoremap <C-b> <C-o>b
inoremap <C-e> <C-o>e
"}}}
" remap ESC to <Ctrl-c>"{{{
inoremap <C-c> <Esc>
"}}}
" Hard to type things"{{{
" imap >> →
" imap << ←
" imap ^^ ↑
" imap VV ↓
"}}}
" Disable highlight when <leader><cr> is pressed"{{{
map <silent> <leader><cr> :noh<cr>
"}}}
" Pressing <Leader>ss will toggle and untoggle spell checking"{{{
map <leader>ss :setlocal spell!<cr>
"}}}
" Force to use Vim motion keys all the time!!!"{{{
nnoremap <up> <nop>
nnoremap <down> <nop>
nnoremap <left> <nop>
nnoremap <right> <nop>
inoremap <up> <nop>
inoremap <down> <nop>
inoremap <left> <nop>
inoremap <right> <nop>
"}}}
"switch between buffers with <C-H> and <C-L>"{{{
map <c-h> :bp<cr>
map <c-l> :bn<cr>
"}}}
" shifting indentation in visual mode"{{{
vnoremap < <gv
vnoremap > >gv
"}}}
" Stay chill <F1>"{{{
noremap! <F1> <Esc>
"}}}
" format JSON"{{{
nmap <leader>jt <Esc>:%!python -m json.tool<CR><Esc>:set filetype=json<CR>
"}}}
" Damn you, Ex mode!!"{{{
nnoremap Q <nop>
"}}}
" Drag selected block"{{{
" have to use the below crazy looking keys to make them work in iTerm2
noremap ∆ :m+<CR>
noremap ˚ :m-2<CR>
noremap ˙ <<
noremap ¬ >>
vnoremap ∆ :m'>+<CR>gv
vnoremap ˚ :m-2<CR>gv
vnoremap ˙ <gv
vnoremap ¬ >gv
"}}}
"}}}

" Self defined functions and stuffs"{{{
" Highlight trailing whitespace"{{{
let b:trailingWhitespace_status = 0
function! TrailingWhitespace()
    if(b:trailingWhitespace_status == 0)
        highlight ExtraWhitespace ctermbg=red guibg=red
        match ExtraWhitespace /\s\+$/
        let b:trailingWhitespace_status = 1
        echo "CHeck"
    else
        highlight ExtraWhitespace ctermbg=none guibg=none
        match ExtraWhitespace /\s\+$/
        let b:trailingWhitespace_status = 0
    endif
endfunc

" nnoremap <silent> <leader>ss :call TrailingWhitespace()<cr>

"}}}
" remove trailing whitespaces"{{{
function! FixTrailingWhitespace()
    exe "normal mZ"
    %s/\s\+$//ge
    exe "normal `Z"
endfunc
command! FixTrailings call FixTrailingWhitespace()

"}}}
" Toggle special characters"{{{
nnoremap <silent> <leader>ts :set list!<cr>
"}}}
" Pandoc grid tablelize"{{{
command! -range=% Rst :'<,'>!pandoc -f markdown -t rst
"}}}
" Highlight over length"{{{
highlight OverLength ctermbg=105 ctermfg=white guibg=#FF00DD
call matchadd('OverLength', '\%81v', 100)
"}}}
" key bindings for Relative Line Numbers"{{{
function! NumberToggle()
  if(&relativenumber == 1)
    set nornu
  else
    set rnu
  endif
endfunc

" toggle relative number
nnoremap <leader>nm :call NumberToggle()<cr>
"}}}
" Uncomment the following to have Vim jump to the last position when"{{{
" reopening a file
if has("autocmd")
 au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$")
   \| exe "normal g'\"" | endif
endif
"}}}
" Don't close window, when deleting a buffer"{{{
command! Bclose call <SID>BufcloseCloseIt()
function! <SID>BufcloseCloseIt()
   let l:currentBufNum = bufnr("%")
   let l:alternateBufNum = bufnr("#")

   if buflisted(l:alternateBufNum)
     buffer #
   else
     bnext
   endif

   if bufnr("%") == l:currentBufNum
     new
   endif

   if buflisted(l:currentBufNum)
     execute("bdelete! ".l:currentBufNum)
   endif
endfunction
"}}}
" vimgrep searching and cope displaying"{{{
"
" When you press gv you vimgrep after the selected text
vnoremap <silent> gv :call VisualSelection('gv')<CR>

" Open vimgrep and put the cursor in the right position
map <leader>g :vimgrep // **/*.<left><left><left><left><left><left><left>

" Vimgreps in the current file
map <leader><space> :vimgrep // <C-R>%<C-A><right><right><right><right><right><right><right><right><right>

" When you press <leader>r you can search and replace the selected text
vnoremap <silent> <leader>r :call VisualSelection('replace')<CR>

map <leader>cp :botright cope<cr>
map <leader>co ggVGy:tabnew<cr>:set syntax=qf<cr>pgg
map <leader>n :cn<cr>
map <leader>p :cp<cr>

function! CmdLine(str)
    exe "menu Foo.Bar :" . a:str
    emenu Foo.Bar
    unmenu Foo
endfunction

function! VisualSelection(direction) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$", "", "")

    if a:direction == 'b'
        execute "normal ?" . l:pattern . "^M"
    elseif a:direction == 'gv'
        call CmdLine(":silent vimgrep " . '/'. l:pattern . '/' . ' **/*.')
    elseif a:direction == 'replace'
        call CmdLine("%s" . '/'. l:pattern . '/')
    elseif a:direction == 'f'
        execute "normal /" . l:pattern . "^M"
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction
"}}}
" Toggle diff this"{{{
nnoremap <silent> <Leader>df :call DiffToggle()<CR>

function! DiffToggle()
    if &diff
        diffoff
    else
        diffthis
    endif
endfunction
"}}}
" Align by | (bar)"{{{
" inoremap <silent> <Bar>   <Bar><Esc>:call <SID>align()<CR>a
 
function! s:align()
  let p = '^\s*|\s.*\s|\s*$'
  if exists(':Tabularize') && getline('.') =~# '^\s*|' && (getline(line('.')-1) =~# p || getline(line('.')+1) =~# p)
    let column = strlen(substitute(getline('.')[0:col('.')],'[^|]','','g'))
    let position = strlen(matchstr(getline('.')[0:col('.')],'.*|\s*\zs.*'))
    Tabularize/|/l1
    normal! 0
    call search(repeat('[^|]*|',column).'\s\{-\}'.repeat('.',position),'ce',line('.'))
  endif
endfunction
"}}}
" convert bar-structured table into pandoc"{{{
 vnoremap <leader>ta :call <SID>table()<cr>
  function! s:table() range
     exe "'<,'>Tab /<bar>"
     let hsepline= substitute(getline("."),'[^|]','-','g')
     exe "norm! o" .  hsepline
     exe "'<,'>s/-|/ |/g"
     exe "'<,'>s/|-/| /g"
     exe "'<,'>s/^| \\|\\s*|$\\||//g"
  endfunction
"}}}
" Toggle TableMode"{{{
nnoremap <leader><Bar> :call ToggleTableMode()<cr>

let g:tableMode = 0
function! ToggleTableMode()
    if g:tableMode==0
        exec ":TableModeEnable"
        echo "Table Mode: On"
        let g:tableMode = 1
    else
        exec ":TableModeDisable"
        echo "Table Mode: Off"
        let g:tableMode = 0
    endif
endfunction
"}}}
" Ignore AngularJS directives in HTML"{{{
let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute \"ng-"]
"}}}
" Ignore chop.js directives in HTML"{{{
let g:syntastic_html_tidy_ignore_errors=[" proprietary attribute \"ch-"]
"}}}
"}}}
"
" Plugins"{{{
" Ident Guides settings"{{{
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1
"}}}
" Tag list?"{{{
let Tlist_Show_One_File = 1
let g:tlist_coffee_settings = 'coffee;f:function;v:variable'
"}}}
" Neocomplcache"{{{
" Disable AutoComplPop.
let g:acp_enableAtStartup = 0
" Use neocomplcache.
let g:neocomplcache_enable_at_startup = 1
" Use smartcase.
let g:neocomplcache_enable_smart_case = 1
" Set minimum syntax keyword length.
let g:neocomplcache_min_syntax_length = 3
let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'
" Force to replace completefunc with neocomplcache
let g:neocomplcache_force_overwrite_completefunc = 1
"}}}
" YankRing"{{{
let g:yankring_history_dir = '~/.vim/tmp/yankring'
nnoremap <leader><leader>p :YRShow<cr>
"}}}
" SuperTab"{{{
let g:SuperTabRetainCompletionType=2
let g:SuperTabDefaultCompletionType="<C-X><C-O>"
" let g:SuperTabMappingBackward = '<s-space>'
"}}}
" TagBar "{{{
nmap <leader>B :TagbarToggle<cr>
let g:tagbar_width = 30

" Markdown support
" let g:tagbar_type_markdown = {
"             \ 'ctagstype' : 'markdown',
"             \ 'kinds' : [
"                 \ 'h:headings',
"                 \ 'l:links',
"                 \ 'i:images'
"             \ ],
"     \ "sort" : 0
" \ }
"
" Add support for markdown files in tagbar.
let g:tagbar_type_markdown = {
    \ 'ctagstype': 'markdown',
    \ 'ctagsbin' : '~/dotfiles/bin/markdown2ctags/markdown2ctags.py',
    \ 'ctagsargs' : '-f - --sort=yes',
    \ 'kinds' : [
        \ 'h:headings',
        \ 's:sections',
        \ 'i:images'
    \ ],
    \ 'sro' : '|',
    \ 'kind2scope' : {
        \ 's' : 'section',
    \ },
    \ 'sort': 0,
\ }

let g:tagbar_type_pandoc = {
            \ 'ctagstype' : 'markdown',
            \ 'kinds' : [
                \ 'h:headings',
                \ 'l:links',
                \ 'i:images'
            \ ],
    \ "sort" : 0
\ }

" Support Vimwiki
let g:tagbar_type_vimwiki = {
            \ 'ctagstype' : 'vimwiki',
            \ 'kinds'     : [
            \ 'h:header',
            \ ],
            \ 'sort'    : 0
            \ }
"}}}
" Vimwiki"{{{
let vimwiki_path=$HOME.'/Dropbox/vimwiki/'
let vimwiki_html_path=$HOME.'/vimwiki_html/'
let g:vimwiki_list = [{'path':vimwiki_path,
                       \ 'path_html':vimwiki_html_path,
                       \ 'template_path':vimwiki_html_path.'vimwiki-assets/',
                       \ 'template_default': 'default',
                       \ 'template_ext': '.tpl',
                       \ 'auto_export': 0}]

" let g:vimwiki_list = [{
"             \ 'path' : '$HOME/Dropbox/vimwiki',
"             \ 'path_html' : '$HOME/vimwiki_html',
"             \ 'nested_syntaxes' : {
"                 \ 'python' : 'python',
"                 \ 'c++' : 'cpp',
"                 \ 'c' : 'c',
"                 \ 'html' : 'html',
"                 \ 'js' : 'js',
"                 \ 'css' : 'css',
"                 \ 'java' : 'java'
"                 \ }
"             \}]

let g:vimwiki_camel_case = 0
let g:vimwiki_hl_cb_checked = 1

autocmd FileType vimwiki map <buffer> <leader>wt :VimwikiTable<cr>
autocmd FileType vimwiki map <buffer> <leader>wa :VimwikiAll2HTML<cr>
"}}}
" Toggle NERDTree by <leader>T"{{{
map <leader>T :NERDTreeToggle<cr>
"}}}
" bind key for CSS3 prefixer under Visual mode"{{{
noremap <leader>css :Prefixer1<cr>
"}}}
" bind <c-p> and <c-n> for Sherlock Commandline Complete"{{{
" cnoremap <C-P> <C-\>esherlock#completeBackward()<CR><c-e>
" cnoremap <C-N> <C-\>esherlock#completeForward()<CR><c-e>
"}}}
" Bind <Leader>a-X for Tabularize"{{{
noremap <Leader>a= :Tabularize /=<cr>
noremap <Leader>a: :Tabularize /:<cr>
noremap <Leader>a, :Tabularize /,<cr>
noremap <Leader>a" :Tabularize /"<cr>
noremap <Leader>a& :Tabularize /&<cr>
"}}}
" toggle TaskList"{{{
" solve a conflict first
nnoremap <leader>v <Plug>TaskList
nnoremap <leader>td :TaskList<CR>
"}}}
" Toggle Gundo by U"{{{
nnoremap U :GundoToggle<CR>
"}}}
" Trigger Numbers toggle by <leader>nn"{{{
nnoremap <leader>nn :NumbersToggle<cr>
"}}}
" JS Context Coloring"{{{
let g:js_context_colors_enabled = 0
nnoremap <leader>cc :JSContextColorToggle<cr>
"}}}
" Javascript Libraries Syntax"{{{
let g:used_javascript_libs = 'jquery,underscore,angularjs'
"}}}
" LaTeX-Box"{{{
let g:LatexBox_viewer="open -a Preview"
"}}}
" NerdTree"{{{
" I still prefer the old-school + - indicators
" let NERDTreeDirArrows=0
"}}}
" Sparkup"{{{
let g:sparkupNextMapping = '<c-x>'
"}}}
" CtrlP
let g:ctrlp_map = '<c-\>'
let g:ctrlp_cmd = 'CtrlP'

"}}}


