# Path to your oh-my-zsh configuration.
ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load.
# Look in ~/.oh-my-zsh/themes/
# Optionally, if you set this to "random", it'll load a random theme each
# time that oh-my-zsh is loaded.
ZSH_THEME="clean"
# ZSH_THEME="agnoster"
# ZSH_THEME="amuse"
# ZSH_THEME="robbyrussell"

# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# Set to this to use case-sensitive completion
# CASE_SENSITIVE="true"

# Comment this out to disable bi-weekly auto-update checks
# DISABLE_AUTO_UPDATE="true"

# Uncomment to change how many often would you like to wait before auto-updates occur? (in days)
# export UPDATE_ZSH_DAYS=13

# Uncomment following line if you want to disable colors in ls
# DISABLE_LS_COLORS="true"

# Uncomment following line if you want to disable autosetting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment following line if you want red dots to be displayed while waiting for completion
# COMPLETION_WAITING_DOTS="true"

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# plugins=(git osx python urltools)

source $ZSH/bin/*.zsh
source $ZSH/oh-my-zsh.sh

# Customize to your needs...
export PATH=/Users/feifeihang/.rvm/gems/ruby-1.9.3-p194/bin:/Users/feifeihang/.rvm/gems/ruby-1.9.3-p194@global/bin:/Users/feifeihang/.rvm/rubies/ruby-1.9.3-p194/bin:/Users/feifeihang/.rvm/bin:/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/texbin:/usr/X11/bin:/Users/feifeihang/.rvm/bin

source ~/.aliases
source ~/.exports
source ~/.functions


# setopt prompt_subst
# autoload -U promptinit
# promptinit 
# prompt physos green black blue black

# # make ls being automatically executed right away after each cd
# function cd()
# {
#     builtin cd "$*" && pwd
# }


PATH=$PATH:$HOME/.rvm/bin # Add RVM to PATH for scripting

NODE_PATH=/usr/local/lib/node_modules

bindkey -e
bindkey '^[[1;9C' forward-word
bindkey '^[[1;9D' backward-word


# bring in z
. ~/Bin/z/z/z.sh



# Vi-mode
# enable vim mode on commmand line
# bindkey -v

# no delay entering normal mode
# https://coderwall.com/p/h63etq
# https://github.com/pda/dotzsh/blob/master/keyboard.zsh#L10
# 10ms for key sequences
KEYTIMEOUT=1

# show vim status
# http://zshwiki.org/home/examples/zlewidgets
# function zle-line-init zle-keymap-select {
#     RPS1="${${KEYMAP/vicmd/-- NORMAL --}/(main|viins)/-- INSERT --}"
#     RPS2=$RPS1
#     zle reset-prompt
# }
# zle -N zle-line-init
# zle -N zle-keymap-select

# add missing vim hotkeys
# fixes backspace deletion issues
# http://zshwiki.org/home/zle/vi-mode
# bindkey -a u undo
# bindkey -a '^R' redo
# bindkey '^?' backward-delete-char
# bindkey '^H' backward-delete-char

# history search in vim mode
# http://zshwiki.org./home/zle/bindkeys#why_isn_t_control-r_working_anymore
# bindkey -M viins '^s' history-incremental-search-backward
# bindkey -M vicmd '^s' history-incremental-search-backward

